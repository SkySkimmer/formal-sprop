
COQBIN?=

# no builtin rules
MAKEFLAGS+=-rR

_CoqProject: ;
Makefile.coq: _CoqProject
	$(COQBIN)coq_makefile -f $< -o $@

ifeq ($(MAKECMDGOALS),clean)
.PHONY: clean
clean:
	+if [ -f Makefile.coq ]; then \
		$(MAKE) -f Makefile.coq clean; \
		rm -f Makefile.coq Makefile.coq.conf; \
	fi
else
%:: submake ;

.PHONY: submake
submake: Makefile.coq
	$(MAKE) -f $< $(MAKECMDGOALS)
endif
.DEFAULT_GOAL:=all
