Set Primitive Projections.

Inductive univ : Set := UProp | USet.

Variant relevance : Set := Irrelevant | Relevant.

Inductive term : Set :=
| Var (r:relevance) (x:nat)
| Univ (u:univ)
| Pi (r:relevance) (dom:term) (codom:term)
| Lambda (r:relevance) (dom:term) (body:term)
| App (head:term) (arg:term)
.

Fixpoint relevance_of_term t :=
  match t with
  | Var r _ => r
  | Univ _ => Relevant
  | Pi _ _ _ => Relevant
  | Lambda _ _ body => relevance_of_term body
  | App head _ => relevance_of_term head
  end.

Fixpoint neutral t :=
  match t with
  | Var _ _ | Univ _ => True
  | App head _ => neutral head
  | Pi _ _ _ | Lambda _ _ _ => False
  end.

Module Wk.
  Inductive t := id | step : t -> t | lift : t -> t.
  Fixpoint compose l r :=
    match l,r with
    | id, _ => r
    | step l, _ => step (compose l r)
    | lift l, id => lift l
    | lift l, step r => step (compose l r)
    | lift l, lift r => lift (compose l r)
    end.

  Fixpoint var l n :=
    match l with
    | id => n
    | step l => S (var l n)
    | lift l => match n with | 0 => 0 | S n => S (var l n) end
    end.

End Wk.
Import Wk.

Fixpoint wk l t :=
  match t with
  | Var r n => Var r (var l n)
  | Univ u => Univ u
  | Pi r a b => Pi r (wk l a) (wk (lift l) b)
  | Lambda r a b => Lambda r (wk l a) (wk (lift l) b)
  | App head arg => App (wk l head) (wk l arg)
  end.

Definition wk1 := wk (step id).

Record Subst := mkSubst { substVar : relevance -> nat -> term }.

Definition liftSubst s := mkSubst (fun r n => match n with 0 => Var r n | S n => substVar s r n end).

Fixpoint subst s t :=
  match t with
  | Var r n => substVar s r n
  | Univ u => Univ u
  | Pi r a b => Pi r (subst s a) (subst (liftSubst s) b)
  | Lambda r a b => Lambda r (subst s a) (subst (liftSubst s) b)
  | App head arg => App (subst s head) (subst s arg)
  end.

Definition idSubst := mkSubst Var.
Definition consSubst s t := mkSubst (fun r n => match n with 0 => t | S n => substVar s r n end).
Definition sgSubst := consSubst idSubst.

Notation "t [ u ]" := (subst (sgSubst u) t) (at level 20).

Inductive Ctx : Set := eCtx | pushVar (r:relevance) (A:term) (tail:Ctx).

Inductive inCtx (r:relevance) : nat -> term -> Ctx -> Set :=
| here : forall x A ctx, inCtx r x (wk1 A) (pushVar r A ctx)
| there : forall x A ctx, inCtx r x A ctx -> forall r' B, inCtx r (S x) (wk1 A) (pushVar r' B ctx).

Definition relevance_of_univ u :=
  match u with UProp => Irrelevant | USet => Relevant end.

Inductive wfCtx : Ctx -> Set :=
| wfeCtx : wfCtx eCtx
| wfpushVar : forall ctx A r, wfCtx ctx -> isType ctx A r -> wfCtx (pushVar r A ctx)

with isType : Ctx -> term -> relevance -> Set :=
| isType_Univ : forall ctx, wfCtx ctx -> forall u, isType ctx (Univ u) Relevant
| isType_Pi : forall ctx rA rB A B,
    isType ctx A rA ->
    isType (pushVar rA A ctx) B rB ->
    isType ctx (Pi rA A B) rB
| type_isType : forall ctx u A, typed ctx A (Univ u) -> isType ctx A (relevance_of_univ u)

with typed : Ctx -> term -> term -> Set :=
| typed_Var : forall ctx x A r, inCtx r x A ctx -> typed ctx (Var r x) A
| typed_Pi : forall ctx uA uB A B,
    let rA := relevance_of_univ uA in
    typed ctx A (Univ uA) ->
    typed (pushVar rA A ctx) B (Univ uB) ->
    typed ctx (Pi rA A B) (Univ uB) (* yes the univ is always the codom one with just uset/uprop *)
| typed_Lambda : forall ctx r A b B,
    isType ctx A r ->
    typed (pushVar r A ctx) b B ->
    typed ctx (Lambda r A b) B
| typed_App : forall ctx head arg r A B,
    typed ctx head (Pi r A B) ->
    typed ctx arg A ->
    typed ctx (App head arg) (B [ arg ])
| typed_conv : forall ctx t r A B,
    typed ctx t A ->
    convTy ctx r A B ->
    typed ctx t B

with convTy : Ctx -> relevance -> term -> term -> Set :=
| reflTy : forall ctx r A, isType ctx A r -> convTy ctx r A A
| conv_convTy : forall ctx u A B, conv ctx (Univ u) A B -> convTy ctx (relevance_of_univ u) A B
| symTy : forall ctx r A B, convTy ctx r A B -> convTy ctx r B A
| transTy : forall ctx r A B C, convTy ctx r A B -> convTy ctx r B C -> convTy ctx r A C
| Pi_congTy : forall ctx rA rB A B A' B',
    isType ctx A rA ->
    convTy ctx rA A A' ->
    convTy (pushVar rA A ctx) rB B B' ->
    convTy ctx rB (Pi rA A B) (Pi rA A' B')

with conv : Ctx -> term (* type *) -> term -> term -> Set :=
| refl : forall ctx A u, conv ctx A u u
| sym : forall ctx A u v, conv ctx A u v -> conv ctx A v u
| trans : forall ctx A u v w, conv ctx A u v -> conv ctx A v w -> conv ctx A u w
| conv_conv : forall ctx r A B u v,
    conv ctx A u v ->
    convTy ctx r A B ->
    conv ctx B u v
| Pi_cong : forall ctx uA uB A A' B B',
    let rA := relevance_of_univ uA in
    let rB := relevance_of_univ uB in
    isType ctx A rA ->
    conv ctx (Univ uA) A A' ->
    conv (pushVar rA A ctx) (Univ uB) B B' ->
    conv ctx (Univ uB) (Pi rA A B) (Pi rA A' B')
| app_cong : forall ctx a b f g A B,
    let r := relevance_of_term a in
    conv ctx (Pi r A B) f g ->
    conv ctx A a b ->
    conv ctx (B [ a ]) (App f a) (App g b)
| beta_conv : forall ctx a t A B,
    let rA := relevance_of_term a in
    isType ctx A rA ->
    typed (pushVar rA A ctx) t B ->
    typed ctx a A ->
    conv ctx (B [ a ]) (App (Lambda rA A t) a) (t [ a ])
(* function eta? *)
| conv_irr : forall ctx A u v,
    isType ctx A Irrelevant -> (* not [typed ctx A (Univ UProp)] as eg [Pi SProp (Var 0)] is irrelevant but untyped *)
    typed ctx u A ->
    typed ctx v A ->
    conv ctx A u v
.

Definition impr_empty := Pi Relevant (Univ UProp) (Var Relevant 0).
Example impr_empty_istype : isType eCtx impr_empty Irrelevant.
Proof.
  unfold impr_empty.
  constructor.
  { do 2 constructor. }
  change Irrelevant with (relevance_of_univ UProp).
  apply type_isType.
  constructor.
  exact (here _ _ (Univ UProp) _).
Defined.
